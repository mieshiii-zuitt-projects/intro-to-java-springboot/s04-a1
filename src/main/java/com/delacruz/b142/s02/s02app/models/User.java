package com.delacruz.b142.s02.s02app.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {

   // Properties (columns)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; //primary key
    @Column
    private String username;
    @Column
    private String password;
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    private Set<Post> posts;

    // Constructors
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // Getters and Setters
    // Getters
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    // Setter
    public void setUsername(String newUsername) {
        this.username = newUsername;
    }

    public void setPassword(String newPassword) {
        this.password = newPassword;
    }

    public Set<Post> getPosts(){
        return posts;
    }
    // Methods

}
